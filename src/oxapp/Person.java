/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxapp;

import java.io.Serializable;

/**
 *
 * @author informatics
 */
public class Person implements Serializable{
    private String FirstName;
    private String Surname;
    private String Username;
    private String Password;
    private String Phone;
    private int Weight;
    private int Height;

    public Person(String FirstName, String Surname, String Username, String Password, String Phone, int Weight, int Height) {
        this.FirstName = FirstName;
        this.Surname = Surname;
        this.Username = Username;
        this.Password = Password;
        this.Phone = Phone;
        this.Weight = Weight;
        this.Height = Height;
    }
    public Person(){
        
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public int getWeight() {
        return Weight;
    }

    public void setWeight(int Weight) {
        this.Weight = Weight;
    }

    public int getHeight() {
        return Height;
    }

    public void setHeight(int Height) {
        this.Height = Height;
    }

    @Override
    public String toString() {
        return "Person{" + "FirstName=" + FirstName + ", Surname=" + Surname + ", Username=" + Username + ", Password=" + Password + ", Phone=" + Phone + ", Weight=" + Weight + ", Height=" + Height + '}';
    }
    
}
